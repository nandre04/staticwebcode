package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testDecreasecounterZero() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Zero", 1, k);
        
    }

   
    @Test
    public void testDecreasecounterOne() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("One", 1, k);

    }

    
    @Test
    public void testDecreasecounterNumber() throws Exception {

        int k= new Increment().decreasecounter(5);
        assertEquals("Number", 1, k);

    }
}
